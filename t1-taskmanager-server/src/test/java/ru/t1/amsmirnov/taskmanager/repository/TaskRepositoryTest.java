package ru.t1.amsmirnov.taskmanager.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ITaskDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final ProjectDTO projectUserAlfa = new ProjectDTO();

    @NotNull
    private SqlSession connection;

    @NotNull
    private List<TaskDTO> tasks;

    @NotNull
    private List<TaskDTO> alfaTasks;

    @NotNull
    private List<TaskDTO> betaTasks;

    @NotNull
    private ITaskDtoRepository taskRepository;

    @NotNull EntityManager entityManager;

    @NotNull EntityTransaction transaction;

    @BeforeClass
    public static void initData() {
        projectUserAlfa.setName("Test task name");
        projectUserAlfa.setDescription("Test task description");
        projectUserAlfa.setUserId(USER_ALFA_ID);
    }

    @Before
    public void initRepository() throws Exception {
        tasks = new ArrayList<>();
        entityManager = CONNECTION_SERVICE.getEntityManager();
        transaction = entityManager.getTransaction();

        taskRepository = connection.getMapper(ITaskDtoRepository.class);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("TaskDTO : " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("Description: " + i);
            if (i <= 5)
                task.setUserId(USER_ALFA_ID);
            else
                task.setUserId(USER_BETA_ID);
            if (i % 3 == 0)
                task.setProjectId(projectUserAlfa.getId());
            try {

                tasks.add(task);
                taskRepository.add(task);
            } catch (final Exception e) {
                transaction.rollback();
                throw e;
            }
        }
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() throws Exception {
        try {
            taskRepository.removeAll();
            tasks.clear();
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }


    @Test
    public void testAdd() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test task name");
        task.setDescription("Test task description");
        task.setUserId(USER_ALFA_ID);
        try {
            taskRepository.add(task);
            tasks.add(task);
            List<TaskDTO> actualTasks = taskRepository.findAll();
            assertEquals(tasks, actualTasks);
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAll() throws Exception {
        try {
            assertNotEquals(0, taskRepository.findAll().size());
            taskRepository.removeAll();
            assertEquals(0, taskRepository.findAll().size());
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        try {
            assertNotEquals(0, taskRepository.findAll(USER_ALFA_ID).size());
            taskRepository.removeAll(USER_ALFA_ID);
            assertEquals(0, taskRepository.findAll(USER_ALFA_ID).size());
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAll() throws Exception {
        try {
            assertNotEquals(taskRepository.findAll(USER_ALFA_ID), taskRepository.findAll(USER_BETA_ID));

            assertEquals(alfaTasks, taskRepository.findAll(USER_ALFA_ID));
            assertEquals(betaTasks, taskRepository.findAll(USER_BETA_ID));
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    @Ignore
    public void testFindAllComparator() throws Exception {
        try {
            alfaTasks.sort(TaskSort.BY_NAME.getComparator());
            assertEquals(alfaTasks, taskRepository.findAllSorted(USER_ALFA_ID, "name"));
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneById() throws Exception {
        try {
            for (final TaskDTO task : tasks) {
                assertEquals(task, taskRepository.findOneById(task.getUserId(), task.getId()));
            }
            assertNull(taskRepository.findOneById(NONE_ID));
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }


    @Test
    public void testFindAllByProjectId() throws Exception {
        final List<TaskDTO> alfaTasksWithProject = alfaTasks
                .stream()
                .filter(t -> projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<TaskDTO> betaTasksWithProject = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID) && projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        try {
            final List<TaskDTO> alfaRepoTasks = taskRepository.findAllByProjectId(USER_ALFA_ID, projectUserAlfa.getId());
            final List<TaskDTO> betaRepoTasks = taskRepository.findAllByProjectId(USER_BETA_ID, projectUserAlfa.getId());
            assertNotEquals(alfaRepoTasks, betaRepoTasks);
            assertEquals(alfaTasksWithProject, alfaRepoTasks);
            assertEquals(betaTasksWithProject, betaRepoTasks);

            assertEquals(new ArrayList<>(), taskRepository.findAllByProjectId(USER_ALFA_ID, NONE_ID));
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testUpdate() throws Exception {
        assertEquals(tasks.get(0), taskRepository.findOneById(tasks.get(0).getId()));
        tasks.get(0).setName("UPDATED_NAME");
        taskRepository.update(tasks.get(0));
        assertEquals(tasks.get(0).getName(), taskRepository.findOneById(tasks.get(0).getId()).getName());
    }

}
