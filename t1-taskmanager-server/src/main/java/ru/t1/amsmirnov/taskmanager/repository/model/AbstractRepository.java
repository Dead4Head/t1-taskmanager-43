package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IAbstractRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
