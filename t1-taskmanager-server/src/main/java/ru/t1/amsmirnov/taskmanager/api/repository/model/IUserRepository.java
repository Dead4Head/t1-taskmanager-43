package ru.t1.amsmirnov.taskmanager.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

public interface IUserRepository extends IAbstractRepository<User> {


    @Nullable
    User findOneByLogin(@Nullable String login);

    @Nullable
    User findOneByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

}
