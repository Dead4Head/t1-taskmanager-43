package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IUserRepository;
import ru.t1.amsmirnov.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        final String jql = "SELECT u FROM User u ORDER BY u.created";
        return entityManager.createQuery(jql, User.class).getResultList();
    }

    @NotNull
    @Override
    public List<User> findAllSorted(@Nullable final String sort) {
        final String jql = "SELECT u FROM User u ORDER BY :sort";
        return entityManager.createQuery(jql, User.class)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) {
        final String jql = "SELECT u FROM User u WHERE u.id = :id";
        return entityManager.createQuery(jql, User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeAll() {
        final String jql = "DELETE FROM User";
        entityManager.createQuery(jql, User.class).executeUpdate();
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        final String jql = "SELECT u FROM User u WHERE u.login = :login";
        return entityManager.createQuery(jql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@Nullable final String email) {
        final String jql = "SELECT u FROM UserDTO u WHERE u.email = :email";
        return entityManager.createQuery(jql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        final String jql = "SELECT COUNT(u) FROM User u WHERE u.login = :login";
        final Long count = entityManager.createQuery(jql, Long.class)
                .setParameter("login", login)
                .getSingleResult();
        return count > 0;
    }

    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        final String jql = "SELECT COUNT(u) FROM User u WHERE u.email = :email";
        final Long count = entityManager.createQuery(jql, Long.class)
                .setParameter("email", email)
                .getSingleResult();
        return count > 0;
    }

}
