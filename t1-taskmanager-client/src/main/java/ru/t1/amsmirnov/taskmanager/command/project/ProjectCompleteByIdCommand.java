package ru.t1.amsmirnov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.project.ProjectCompleteByIdRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.ProjectCompleteByIdResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-id";


    @NotNull
    public static final String DESCRIPTION = "Complete project by ID.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken(), id);
        @NotNull final ProjectCompleteByIdResponse response = getProjectEndpoint().completeProjectById(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
