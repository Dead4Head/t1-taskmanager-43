package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse() {
    }

    public TaskRemoveByIdResponse(@Nullable final TaskDTO task) {
        super(task);
    }

    public TaskRemoveByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}