package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest() {
    }

    public TaskListByProjectIdRequest(
            @Nullable final String token,
            @Nullable final String projectId
    ) {
        super(token);
        setProjectId(projectId);
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}