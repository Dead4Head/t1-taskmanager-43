package ru.t1.amsmirnov.taskmanager.enumerated.database;

import org.jetbrains.annotations.NotNull;

public enum UserConst {

    COLUMN_LOGIN("login"),
    COLUMN_PASSWORD("password"),
    COLUMN_LST_NAME("lst_name"),
    COLUMN_FST_NAME("fst_name"),
    COLUMN_MID_NAME("mid_name"),
    COLUMN_LOCKED("locked"),
    COLUMN_EMAIL("email");

    @NotNull
    private final String columnName;

    UserConst(@NotNull final String columnName) {
        this.columnName = columnName;
    }

    @NotNull
    public String getColumnName() {
        return columnName;
    }

}
