package ru.t1.amsmirnov.taskmanager.enumerated.database;

import org.jetbrains.annotations.NotNull;

public enum DBCost {

    TABLE_PROJECT("tm_project"),
    TABLE_TASK("tm_task"),
    TABLE_USER("tm_user"),
    TABLE_SESSION("tm_session"),

    COLUMN_ID("id"),
    COLUMN_NAME("name"),
    COLUMN_STATUS("status"),
    COLUMN_CREATED("created"),
    COLUMN_DESCRIPTION("description"),
    COLUMN_ROLE("role"),


    COLUMN_USER_ID("user_id"),
    COLUMN_PROJECT_ID("project_id");

    @NotNull
    private final String displayName;

    DBCost(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
