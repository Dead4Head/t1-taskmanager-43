package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataBackupLoadResponse extends AbstractResultResponse {

    public DataBackupLoadResponse () {
    }

    public DataBackupLoadResponse (@NotNull final Throwable throwable) {
        super(throwable);
    }

}
