package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse() {
    }

    public UserRemoveResponse(@Nullable final UserDTO user) {
        super(user);
    }

    public UserRemoveResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}